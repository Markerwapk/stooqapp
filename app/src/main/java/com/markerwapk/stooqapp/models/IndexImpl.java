package com.markerwapk.stooqapp.models;

import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;

import com.markerwapk.stooqapp.R;
import com.markerwapk.stooqapp.ResultCallback;
import com.markerwapk.stooqapp.StooqApplication;
import com.markerwapk.stooqapp.rest.StooqRest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Markerwapk on 18.08.16.
 */
public abstract class IndexImpl implements Index {

    private static final String TAG = "IndexImpl";

    private static final int CLOSE_VALUE_INDEX = 6;

    private static final int GET_INDEX_VALUE = 0;

    public static final int STATUS_FALL_AWAY = -1;
    public static final int STATUS_SAME = 0;
    public static final int STATUS_RISE = 1;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

    @Inject
    StooqRest stooqRest;

    private String value = "";

    private String time;

    private final StooqApplication application;

    private final ResultCallback resultCallback;

    private int status;

    public IndexImpl(StooqApplication application, ResultCallback resultCallback) {
        this.application = application;
        this.resultCallback = resultCallback;

        application.getRestComponent().inject(this);

        value = application.getString(R.string.no_data);
        time = getFormattedTime(System.currentTimeMillis());

    }


    @Override
    public String getTime() {
        return time;
    }

    @Override
    public String getValue() {
        return value;
    }

    protected abstract String getUrl();

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {

            Log.d(TAG, "handleMessage()" + handler);
            stooqRest.getWig(getUrl()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        writeResponseBodyToDisk(response.body().byteStream());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {


                    responseFailed();
                }
            });
            return false;
        }
    });

    private void writeResponseBodyToDisk(InputStream inputStream) {
        BufferedReader r = null;
        try {
            r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder response = new StringBuilder();
            String line;

            while ((line = r.readLine()) != null) {
                response.append(line).append('\n');
            }


            parseResponse(response.toString());


        } catch (IOException e) {
            e.printStackTrace();

            responseFailed();
        } finally {

            if (r != null) {
                try {
                    r.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    private void parseResponse(String s) {
        String[] lines = s.split("\\n");

        if (lines.length == 2) {
            String[] fields = lines[1].split(",");

            if (fields.length == 8) {
                String newValueString = String.valueOf(fields[CLOSE_VALUE_INDEX]);

                double oldValue = getNumeric(value);
                double newValue = getNumeric(newValueString);

                if (oldValue == newValue) {
                    status = STATUS_SAME;
                } else if (oldValue > newValue) {
                    status = STATUS_FALL_AWAY;
                } else {
                    status = STATUS_RISE;
                }

                value = newValueString;
                notifyView();
            } else {
                responseFailed();
            }
        } else {
            responseFailed();
        }

    }

    private double getNumeric(String str) {
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }

    private void responseFailed() {
        value = application.getString(R.string.response_failed);
        notifyView();
    }


    private void notifyView() {
        time = getFormattedTime(System.currentTimeMillis());

        if (resultCallback != null) {
            resultCallback.onChangeValue();
        }

        handler.sendEmptyMessageDelayed(GET_INDEX_VALUE,
                Integer.parseInt(
                        PreferenceManager.getDefaultSharedPreferences(application)
                                .getString(application.getString(R.string.interval_time_key)
                                        , application.getString(R.string.default_interval_time))
                ) * 1000);
    }


    private String getFormattedTime(long time) {
        return simpleDateFormat.format(time);
    }

    @Override
    public void stopRestRequests() {
        handler.removeMessages(GET_INDEX_VALUE);
    }

    @Override
    public void startRestRequests() {
        handler.sendEmptyMessage(GET_INDEX_VALUE);
    }

    @Override
    public int getStatus() {
        return status;
    }
}
