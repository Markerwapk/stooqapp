package com.markerwapk.stooqapp.models;

import com.markerwapk.stooqapp.R;
import com.markerwapk.stooqapp.ResultCallback;
import com.markerwapk.stooqapp.StooqApplication;

/**
 * Created by Markerwapk on 18.08.16.
 */
public class WIG40 extends IndexImpl {

    public WIG40(StooqApplication applicationContext, ResultCallback resultCallback) {
        super(applicationContext, resultCallback);
    }

    @Override
    protected String getUrl() {
        return "/q/l/?s=mwig40&f=sd2t2ohlcv&h&e=csv";
    }

    @Override
    public int getTitle() {
        return R.string.mwig40_title;
    }
}
