package com.markerwapk.stooqapp.models;

import com.markerwapk.stooqapp.R;
import com.markerwapk.stooqapp.ResultCallback;
import com.markerwapk.stooqapp.StooqApplication;

/**
 * Created by Markerwapk on 18.08.16.
 */
public class WIG80 extends IndexImpl {
    public WIG80(StooqApplication applicationContext, ResultCallback resultCallback) {
        super(applicationContext, resultCallback);
    }

    @Override
    protected String getUrl() {
        return "/q/l/?s=swig80&f=sd2t2ohlcv&h&e=csv";
    }

    @Override
    public int getTitle() {
        return R.string.swig80_title;
    }
}
