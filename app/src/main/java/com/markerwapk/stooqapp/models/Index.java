package com.markerwapk.stooqapp.models;

/**
 * Created by Markerwapk on 18.08.16.
 */
public interface Index {
    int getTitle();

    String getValue();

    String getTime();

    void stopRestRequests();

    void startRestRequests();

    int getStatus();
}
