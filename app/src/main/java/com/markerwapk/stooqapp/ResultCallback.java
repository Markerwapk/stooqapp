package com.markerwapk.stooqapp;

/**
 * Created by Markerwapk on 18.08.16.
 */
public interface ResultCallback {
    void onChangeValue();
}
