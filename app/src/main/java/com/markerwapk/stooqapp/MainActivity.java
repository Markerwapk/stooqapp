package com.markerwapk.stooqapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.markerwapk.stooqapp.models.Index;
import com.markerwapk.stooqapp.models.WIG;
import com.markerwapk.stooqapp.models.WIG20;
import com.markerwapk.stooqapp.models.WIG40;
import com.markerwapk.stooqapp.models.WIG80;
import com.markerwapk.stooqapp.rest.StooqRest;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Inject
    StooqRest stooqRest;

    @BindView(R.id.result_list)
    RecyclerView resultList;

    private final ArrayList<Index> indexes = new ArrayList<>();
    private IndexesAdapter indexesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);


        prepareRecyclerView();
        prepareIndexes();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    protected void onResume() {
        super.onResume();

        startRestRequests();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopRestRequests();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.preferences: {
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void stopRestRequests() {
        for (Index index : indexes) {
            index.stopRestRequests();
        }
    }

    private void startRestRequests() {
        for (Index index : indexes) {
            index.startRestRequests();
        }
    }

    private void prepareRecyclerView() {
        resultList.setLayoutManager(new GridLayoutManager(this, 2));

        indexesAdapter = new IndexesAdapter(indexes);
        resultList.setAdapter(indexesAdapter);
    }

    private void prepareIndexes() {
        indexes.add(new WIG((StooqApplication) getApplication(), getResultCallback(indexes.size())));
        indexes.add(new WIG20((StooqApplication) getApplication(), getResultCallback(indexes.size())));
        indexes.add(new WIG40((StooqApplication) getApplication(), getResultCallback(indexes.size())));
        indexes.add(new WIG80((StooqApplication) getApplication(), getResultCallback(indexes.size())));
    }

    @NonNull
    private ResultCallback getResultCallback(final int position) {
        return new ResultCallback() {
            @Override
            public void onChangeValue() {
                indexesAdapter.notifyItemChanged(position);
            }
        };
    }
}
