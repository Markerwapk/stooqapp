package com.markerwapk.stooqapp.modules;

import com.markerwapk.stooqapp.rest.StooqRest;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by Markerwapk on 18.08.16.
 */

@Module
public class RestModule {

    private final String baseURL;

    public RestModule(String baseURL) {
        this.baseURL = baseURL;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder().build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder().client(okHttpClient).baseUrl(baseURL).build();
    }

    @Provides
    @Singleton
    StooqRest provideStooqRest(Retrofit retrofit) {
        return retrofit.create(StooqRest.class);
    }
}
