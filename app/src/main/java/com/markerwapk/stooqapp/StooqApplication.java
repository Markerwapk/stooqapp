package com.markerwapk.stooqapp;

import android.app.Application;

import com.markerwapk.stooqapp.components.DaggerRestComponent;
import com.markerwapk.stooqapp.components.RestComponent;
import com.markerwapk.stooqapp.modules.RestModule;

/**
 * Created by Markerwapk on 18.08.16.
 */
public class StooqApplication extends Application {

    private static final String BASE_URL = "http://stooq.pl/";

    private RestComponent restComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        restComponent = DaggerRestComponent.builder().restModule(new RestModule(BASE_URL)).build();
    }

    public RestComponent getRestComponent() {
        return restComponent;
    }
}
