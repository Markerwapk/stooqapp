package com.markerwapk.stooqapp;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.markerwapk.stooqapp.models.Index;
import com.markerwapk.stooqapp.models.IndexImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Markerwapk on 18.08.16.
 */
public class IndexesAdapter extends RecyclerView.Adapter<IndexesAdapter.ViewHolder> {
    private final ArrayList<Index> indexes;

    public IndexesAdapter(ArrayList<Index> indexes) {
        this.indexes = indexes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.index_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Index index = indexes.get(position);

        holder.title.setText(index.getTitle());
        holder.value.setText(index.getValue());
        holder.time.setText(index.getTime());

        showNewValueAnimation(holder.mainLayout, index.getStatus());
    }

    private void showNewValueAnimation(final View view, int status) {

        ArgbEvaluator evaluator = new ArgbEvaluator();
        ValueAnimator animator = new ValueAnimator();

        int color;
        switch (status) {
            case IndexImpl.STATUS_RISE:
                color = view.getResources().getColor(R.color.colorRise);
                break;
            case IndexImpl.STATUS_FALL_AWAY:
                color = view.getResources().getColor(R.color.colorFallAway);
                break;
            case IndexImpl.STATUS_SAME:
            default:
                color = view.getResources().getColor(R.color.colorSame);
        }

        animator.setIntValues(Color.parseColor("#FFFFFFFF"), color, Color.parseColor("#FFFFFFFF"));
        animator.setEvaluator(evaluator);
        animator.setDuration(2000);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                       @Override
                                       public void onAnimationUpdate(ValueAnimator animation) {
                                           int color = (int) animation.getAnimatedValue();
                                           view.setBackgroundColor(color);
                                       }
                                   }
        );
        animator.start();

    }

    @Override
    public int getItemCount() {
        return indexes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.result_layout)
        LinearLayout mainLayout;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.value)
        TextView value;

        @BindView(R.id.time)
        TextView time;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
