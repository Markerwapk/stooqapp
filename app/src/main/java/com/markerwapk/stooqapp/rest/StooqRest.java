package com.markerwapk.stooqapp.rest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Markerwapk on 18.08.16.
 */
public interface StooqRest {

    @GET()
    Call<ResponseBody> getWig(@Url String fileURl);
}
