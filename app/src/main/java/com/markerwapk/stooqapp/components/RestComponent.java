package com.markerwapk.stooqapp.components;

import com.markerwapk.stooqapp.models.IndexImpl;
import com.markerwapk.stooqapp.modules.RestModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Markerwapk on 18.08.16.
 */
@Singleton
@Component(modules = RestModule.class)
public interface RestComponent {
    void inject(IndexImpl indexImpl);
}
